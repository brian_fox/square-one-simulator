import React, { useRef, useEffect } from 'react'
import { boardDimensions } from '../settings/board'
import { buildMap, drawGrid } from '../utils/map_tools'
import { initDragscroll } from '../utils/dragscroll'

const Canvas = (props: React.CanvasHTMLAttributes<HTMLCanvasElement>) => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const boardRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const canvas = canvasRef.current
  
    if (!canvas) {
      throw("Can't get the canvas")
    }

    const canvasContext = canvas?.getContext('2d')

    if (!canvasContext) {
      throw("Can't get the context")
    }

    canvas.width = boardDimensions.width
    canvas.height = boardDimensions.height

    buildMap()
    drawGrid(canvasContext)

    const midWidth = (canvas.width/2) - (window.innerWidth/2)
    const midHeight = (canvas.height/2) - (window.innerHeight/2)
    window.scrollTo(midWidth, midHeight)
    initDragscroll(boardRef.current as HTMLElement);
  }, [])

  return (
    <div ref={boardRef} id="gameBoard">
      <canvas ref={canvasRef} {...props}/>
    </div>
  )
}

export default Canvas