import type { NextPage } from 'next'
import Canvas from './canvas'

const Home: NextPage = () => {
  return (
    <Canvas/>
  )
}

export default Home
