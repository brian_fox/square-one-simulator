import { mapGenerationSettings } from "../settings/map";

function roundUp(num: number, precision: number = 0) {
  precision = Math.pow(10, precision)
  return Math.ceil(num * precision) / precision
}

// express noise as a real number between 1 and 0 in intervals of 0.05
function convertToLargeUnitInterval(elevation: number) {
  // not very precise but good enough for needs
  return roundToFiveHundreds(convertToSmallInterval(elevation))
}

// express noise as a real number between 1 and 0 in intervals of 0.025
function convertToSmallUnitInterval(elevation: number) {
  return roundToTwentyFiveThousands(convertToSmallInterval(elevation))
}

function convertToSmallInterval(elevation: number) {
  return roundToTwentyFiveThousands(Math.pow((elevation + 1) /2, mapGenerationSettings.elevationModifier))
}

function roundToFiveHundreds(num:number) {
  return roundUp(num*20, 0)/20;
}

function roundToTwentyFiveThousands(num:number) {
  return roundUp(num*40, 0)/40;
}

export {
  roundToFiveHundreds,
  roundUp,
  convertToLargeUnitInterval,
  convertToSmallUnitInterval
}