
let offsetStart: { left: number, top: number } = { left: 0, top: 0};
let board:HTMLElement | null = null;

const centerBoard = (board: HTMLElement) => {
  board.scrollTop = (board.scrollHeight - board.clientHeight) / 2
  board.scrollLeft = (board.scrollWidth - board.clientWidth) / 2
}

const onMouseMove = (event: MouseEvent) => {
  const options = {
    scrollOutside: false,
    scrollRate: 0.7
  }

  const movedY = Math.abs(event.movementY)
  const movedX = Math.abs(event.movementX)
  const yMultiplier = Math.log(movedY) > 1 ? (Math.log(movedY)) : 1
  const xMultiplier = Math.log(movedX) > 1 ? (Math.log(movedX)) : 1

  if (!board) {
    return;
  }

  board.scrollTop = board.scrollTop - (event.movementY * yMultiplier) * options.scrollRate
  board.scrollLeft = board.scrollLeft - (event.movementX * xMultiplier) * options.scrollRate
}

const onMouseDown = (event: MouseEvent) => {
  const elementName = event.target as HTMLElement

  if (elementName.localName === 'path' || elementName.localName === 'canvas') {
    window.addEventListener('mousemove', onMouseMove)
    offsetStart = { left: event.clientX, top: event.clientY }
  }
}

const onMouseUp = (event: MouseEvent) => {
  window.removeEventListener('mousemove', onMouseMove)

  const xDiff = Math.abs(offsetStart.left - event.clientX)
  const yDiff = Math.abs(offsetStart.top - event.clientY)
}

const makeDragScrollable = (board: HTMLElement) => {
  window.addEventListener('mousedown', onMouseDown)
  window.addEventListener('mouseup', onMouseUp)
}

const initDragscroll = (element:HTMLElement) => {
  board = element
  centerBoard(board)
  makeDragScrollable(board)
}

export { initDragscroll }