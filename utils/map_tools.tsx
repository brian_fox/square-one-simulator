import { boardDimensions } from '../settings/board'
import { mapGenerationSettings } from '../settings/map'
import { terrainTiles, elevations, humidityRanges, biomes, smallIncrements } from '../settings/terrain'
import SimplexNoise from 'simplex-noise'
import { MapCell } from './map_tools.types'
import { convertToLargeUnitInterval, convertToSmallUnitInterval } from './math'

const r = boardDimensions.tileRadius
const a = 2 * Math.PI / 6
const simplexForElevation = new SimplexNoise()
const simplexForHumidity  = new SimplexNoise()
const grid: Array<MapCell[]> = []

function buildMap() {
  const sinA = Math.sin(a)
  const cosA = Math.cos(a)
  const increment = r * (1 + cosA)
  let gridPosY = 0
  let gridPosX = 0

  for (let y = r; y + r * sinA < boardDimensions.height; y += r * sinA) {
    if (grid.length <= gridPosY) {
      const row:Array<MapCell> = []
      grid.push(row)
    }

    for (let x = r, j = 0; x + increment < boardDimensions.width; x += increment, y += (-1) ** j++ * r * sinA) {
      if (grid[gridPosY].length <= gridPosX) {
        const rawElevation       = calculateNoise(x, y, simplexForElevation)
        const rawHumidity        = calculateNoise(x, y, simplexForHumidity )
        const elevationIncrement = convertToLargeUnitInterval(rawElevation)
        let   elevation          = getElevation(elevationIncrement)
        const humidity           = getHumidity (convertToLargeUnitInterval(rawHumidity))

        // if the elevation is set to small increments, split it into ranges of 0.025
        if (smallIncrements[elevationIncrement + ""]) {
          elevation = getElevation(convertToSmallUnitInterval(rawElevation))
        }

        const biome = biomes[elevation + '_' + humidity]

        grid[gridPosY].push({
          x : x,
          y : y,
          elevation : elevation,
          humidity  : humidity,
          biome     : biome
        })
      }

      gridPosX++
    }

    gridPosY++
  }
}

function getElevation (elevation: number) {
  return elevations[elevation]
}

function getHumidity (elevation: number) {
  return humidityRanges[elevation + ""]
}

function drawGrid(canvasContext : CanvasRenderingContext2D) {
  grid.forEach(row => {
    row.forEach((cell:MapCell) => {
      drawHexagon(cell, canvasContext)
    })
  })
}

function drawHexagon(cell: MapCell, canvasContext: CanvasRenderingContext2D) {
  canvasContext.beginPath()

  for (let i = 0; i < 6; i++) {
    canvasContext.lineTo(cell.x + r * Math.cos(a * i), cell.y + r * Math.sin(a * i))
  }

  canvasContext.closePath()
  canvasContext.stroke()
  canvasContext.fillStyle = terrainTiles[cell.biome]
  canvasContext.fill()
}

function calculateNoise (x: number, y: number, simplex: SimplexNoise) {
  const nx = (x/boardDimensions.width - 0.5) * 4, ny = (y/boardDimensions.height - 0.5) * 4
  const amplitude       = mapGenerationSettings.amplitude
  const amplitude2      = amplitude / 2
  const amplitude3      = amplitude2 / 2
  const sumOfAmplitudes = amplitude + amplitude2 + amplitude3

  const noise = (
      amplitude  * simplex.noise2D(nx, ny)
    + amplitude2 * simplex.noise2D(2 * nx, 2 * ny)
    + amplitude3 * simplex.noise2D(4 * nx, 4 * ny)
  ) / sumOfAmplitudes

  return noise;
}

export {
  buildMap,
  drawGrid,
  drawHexagon
}