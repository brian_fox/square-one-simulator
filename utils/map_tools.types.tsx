interface MapCell {
  x: number,
  y: number,
  elevation: number,
  humidity: number,
  biome: string
}

export type {
  MapCell
}