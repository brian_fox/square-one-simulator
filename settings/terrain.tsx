const terrainTiles: any = {
  water      : "#84bdeb",
  sand       : "#f3e5ab",
  dunes      : "#a69a64",
  cactus     : "#7d9a64",
  savana     : "#bcce82",
  desert     : "#d9cc99",
  forest     : "#549636",
  bare_hill  : "#9f853e",
  scrub_hill : "#868f42",
  pine_hill  : "#5c7934",
  grass      : "#84bd55",
  mountain   : "gray",
  snow       : "white"
}

// optionaly break certain ranges down into smaller increments for more control 
const smallIncrements: any = {
  "0.35" : true
}

const elevations: any = {
  "0.05"  : "water",
  "0.1"   : "water",
  "0.15"  : "water",
  "0.2"   : "water",
  "0.25"  : "water",
  "0.3"   : "water",
  "0.325" : "water",
  "0.35"  : "sand",
  "0.4"   : "plain",
  "0.45"  : "plain",
  "0.5"   : "plain",
  "0.55"  : "plain",
  "0.6"   : "plain",
  "0.65"  : "plain",
  "0.7"   : "hill",
  "0.75"  : "hill",
  "0.8"   : "mountain",
  "0.85"  : "mountain",
  "0.9"   : "mountain",
  "0.95"  : "mountain",
  "1"     : "mountain",
}

const humidityRanges: any = {
  "0.05" : "arid",
  "0.1"  : "arid",
  "0.15" : "arid",
  "0.2"  : "arid",
  "0.25" : "arid",
  "0.3"  : "semi_arid",
  "0.35" : "semi_arid",
  "0.4"  : "semi_arid",
  "0.45" : "temperate",
  "0.5"  : "temperate",
  "0.55" : "temperate",
  "0.6"  : "temperate",
  "0.65" : "temperate",
  "0.7"  : "lush",
  "0.75" : "lush",
  "0.8"  : "lush",
  "0.85" : "lush",
  "0.9"  : "lush",
  "0.95" : "lush",
  "1"    : "lush",
}

const biomes: any = {
  water_arid         : "water",
  water_semi_arid    : "water",
  water_temperate    : "water",
  water_lush         : "water",
  sand_arid          : "sand",
  sand_semi_arid     : "sand",
  sand_temperate     : "sand",
  sand_lush          : "sand",
  plain_arid         : "desert",
  plain_semi_arid    : "savana",
  plain_temperate    : "grass",
  plain_lush         : "forest",
  hill_arid          : "bare_hill",
  hill_semi_arid     : "bare_hill",
  hill_temperate     : "scrub_hill",
  hill_lush          : "pine_hill",
  mountain_arid      : "mountain",
  mountain_semi_arid : "mountain",
  mountain_temperate : "mountain",
  mountain_lush      : "snow"
}

export {
  terrainTiles,
  smallIncrements,
  elevations,
  humidityRanges,
  biomes
}